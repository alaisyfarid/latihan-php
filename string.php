<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Belajar String</title>
</head>
<body>
	<h2>Contoh Soal</h2>

	<?php
		echo "<h3> Soal No 1</h3>";
        /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat 
            tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

		echo "Soal 1.a <br>";
		$first_sentence = "Hello PHP!";
		echo "Kalimat 1 : ".$first_sentence. "<br>";
		echo "Panjang String : ". strlen($first_sentence). "<br>";
		echo "Jumlah Kata : ". str_word_count($first_sentence). "<br><br>";
		echo "soal 1.b <br>";
		$second_sentence = "I'm ready for the challenges";
		echo "Kalimat 1 : ".$second_sentence. "<br>";
		echo "Panjang String : ". strlen($second_sentence). "<br>";
		echo "Jumlah Kata : ". str_word_count($second_sentence). "<br>";

		echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: ". substr($string2, 2,5 ) ;
        echo "<br> Kata Ketiga: ". substr($string2, 7,10 ) ;

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but Good!";
        echo "String: \"$string3\" "."<br>";
        echo "Mengubah Kata variabel string 3 : ". str_replace("Good!","awesome", $string3);
        // OUTPUT : "PHP is old but awesome"

	 ?>	
</body>
</html>